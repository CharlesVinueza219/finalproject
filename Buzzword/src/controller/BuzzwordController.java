package controller;

import gui.*;
import javafx.scene.input.KeyCode;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ui.*;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static gui.FirstSceneGUI.usr;

/**
 * @author Charles Vinueza
 */
public class BuzzwordController
{
    String category;
    static GameScreenGUI gameScreenGUI;
    static int targetScore;

    public void handleLoginRequest()
    {
        try {
            InputDialogSingleton input = InputDialogSingleton.getSingleton();
            input.show("Login");
            if (input.getSelection().equals("Submit") || input.getKeyPressedInScene() == KeyCode.ENTER)
            {
                File f = new File("Buzzword/resources/saved/");
                String absolutePath = f.getAbsolutePath();
                File folder = new File(absolutePath);
                File[] listOfFiles = folder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".json");
                    }
                });
                boolean found = false;  //flag to see if the correct pwd is found or
                // to end the loop that searches through JSON usrnames/passwords
                try {
                    JSONParser parser = new JSONParser();
                    for (int i = 0; i < listOfFiles.length && !found; i++) {
                        JSONObject obj = (JSONObject) parser.parse(new FileReader(listOfFiles[i].getAbsolutePath()));
                        String pwd = (String) obj.get("password");
                        String usrName = (String) obj.get("username");

                        //convert typed pwd to md5 hash and check if that md5 hash equals the hash stored in the file
                        MessageDigest md = null;
                        try {
                            md = MessageDigest.getInstance("MD5");
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        md.update(input.getPassword().getBytes());
                        byte[] digest = md.digest();
                        StringBuffer sb = new StringBuffer();
                        for (byte b : digest) {
                            sb.append(String.format("%02x", b & 0xff));
                        }

                        if (sb.toString().equals(pwd) && input.getUserName().equalsIgnoreCase(usrName)) {
                            //this is where we populate the game with the user's game data
                            found = true;
                            FirstSceneGUI.getUserDataFile().loadFileToGame(listOfFiles[i].getAbsolutePath());
                            switchToPostLoginScreen();
                            input.clearInput();
                        }
                        else if(i == listOfFiles.length - 1)
                        {
                            /*InputDialogSingleton again = InputDialogSingleton.getSingleton();
                            again.show("Invalid username/password");
                            again.clearInput();
                            if (again.getPassword().equals(pwd) && again.getUserName().equalsIgnoreCase(usrName)) {
                                //this is where we populate the game with the user's game data
                                found = true;
                                FirstSceneGUI.getUserDataFile().loadFileToGame(listOfFiles[i].getAbsolutePath());
                                switchToPostLoginScreen();
                                again.clearInput();
                            }
                            else if (again.getSelection().equalsIgnoreCase("close")){
                                found = true;
                            }
                            else {
                                again.clearInput();
                            }*/
                            handleLoginRequestAgain();
                        }
                    }
                } catch (ParseException p) {
                    p.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if(input.getSelection().equalsIgnoreCase("Close"))
            {
                input.clearInput();
            }
        }catch(Exception e)
        {
        }
    }
    public void handleLoginRequestAgain()
    {
        try {
            InputDialogSingleton input = InputDialogSingleton.getSingleton();
            input.show("Invalid Username/Password");
            if (input.getSelection().equals("Submit") || input.getKeyPressedInScene() == KeyCode.ENTER)
            {
                File f = new File("Buzzword/resources/saved/");
                String absolutePath = f.getAbsolutePath();
                File folder = new File(absolutePath);
                File[] listOfFiles = folder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".json");
                    }
                });
                boolean found = false;  //flag to see if the correct pwd is found or
                // to end the loop that searches through JSON usrnames/passwords
                try {
                    JSONParser parser = new JSONParser();
                    for (int i = 0; i < listOfFiles.length && !found; i++) {
                        JSONObject obj = (JSONObject) parser.parse(new FileReader(listOfFiles[i].getAbsolutePath()));
                        String pwd = (String) obj.get("password");
                        String usrName = (String) obj.get("username");

                        //convert typed pwd to md5 hash and check if that md5 hash equals the hash stored in the file
                        MessageDigest md = null;
                        try {
                            md = MessageDigest.getInstance("MD5");
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        md.update(input.getPassword().getBytes());
                        byte[] digest = md.digest();
                        StringBuffer sb = new StringBuffer();
                        for (byte b : digest) {
                            sb.append(String.format("%02x", b & 0xff));
                        }

                        if (sb.toString().equals(pwd) && input.getUserName().equalsIgnoreCase(usrName)) {
                            //this is where we populate the game with the user's game data
                            found = true;
                            FirstSceneGUI.getUserDataFile().loadFileToGame(listOfFiles[i].getAbsolutePath());
                            switchToPostLoginScreen();
                            input.clearInput();
                        }
                        else if(i == listOfFiles.length - 1)
                        {
                            handleLoginRequestAgain();
                        }
                    }
                } catch (ParseException p) {
                    p.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if(input.getSelection().equalsIgnoreCase("Close"))
            {
                input.clearInput();
            }
        }catch(Exception e)
        {
        }
    }

    public void handleLogoutRequest()
    {
        if(FirstSceneGUI.getGameData().getGameStateSavable() == false) {
            YesNoCancelDialogSingleton yesNo = YesNoCancelDialogSingleton.getSingleton();
            yesNo.show("!", "Are you sure you want to logout?");
            if (yesNo.getSelection().equalsIgnoreCase("yes")) {
                FirstSceneGUI.getGameData().revertToNullUser();
                FirstSceneGUI.getGamePrimaryStage().setScene(FirstSceneGUI.getScene());
            } else {
                yesNo.close();
            }
        }
        else if(FirstSceneGUI.getGameData().getGameStateSavable() == true) {
            YesNoCancelDialogSingleton yesNo = YesNoCancelDialogSingleton.getSingleton();
            yesNo.show("!", "Are you sure you want to logout?\nAll your saved progress will be saved :) ");
            if (yesNo.getSelection().equalsIgnoreCase("yes")) {
                FirstSceneGUI.getUserDataFile().saveData(FirstSceneGUI.getGameData().getUsrName().toString());
                FirstSceneGUI.getGameData().revertToNullUser();
                FirstSceneGUI.getGamePrimaryStage().setScene(FirstSceneGUI.getScene());
            } else {
                yesNo.close();
            }
        }
    }
    public void handlePasswordChangeRequest()
    {
        SetPasswordSingleton sp = SetPasswordSingleton.getSingleton();
        sp.show("Change Password");
        if(sp.getSelection().equals("Submit")) {
            if(sp.getNewPassword().isEmpty())
            {
                sp.closeDialog();
                AppMessageDialogSingleton err = AppMessageDialogSingleton.getSingleton();
                err.show("!", "Could not change password. Password box may have been empty");
            }
            else {
                FirstSceneGUI.getUserDataFile().setCurrentPassword(sp.getNewPassword());
                sp.closeDialog();
            }
        }
        else if(sp.getSelection().equals("Close"))
        {
            sp.closeDialog();
        }
    }
    public void handleHelpRequest()
    {
        HelpScreenGUI help = new HelpScreenGUI();
        FirstSceneGUI.getGamePrimaryStage().setScene(help.getScene());
    }

    public void handleExitRequest()
    {
        if(FirstSceneGUI.getGameData().getGameStateSavable() == false)
        {
            System.exit(0);
        }
        else {
            YesNoCancelDialogSingleton close = YesNoCancelDialogSingleton.getSingleton();
            close.show("?", "Are you sure you want to exit?\nAll unsaved progress will be saved :)");

            if (close.getSelection().equalsIgnoreCase("yes")) {
                FirstSceneGUI.getUserDataFile().saveData(FirstSceneGUI.getGameData().getUsrName().toString());
                System.exit(0);
            } else {
                close.close();
            }
        }
    }
    public void handleLevelSelectRequest(String level)
    {
        if(level.equals("Select Game Mode"))
        {
        }
        else
        {
            setCategory(level);
            FirstSceneGUI.getGamePrimaryStage().setScene(LevelSelectScreenGUI.getScene());
        }
    }
    public void helpReturnButtonRequest()
    {
        FirstSceneGUI.getGamePrimaryStage().setScene(FirstSceneGUI.getScene());
    }
    public void handleHomeButtonRequest()
    {
        FirstSceneGUI.getGamePrimaryStage().setScene(PostLoginScreenGUI.getScene());

    }
    public void handleCreateProfileRequest() {
        boolean userNameFound = true;

        boolean flag = false;

        boolean closed = false;
        while(userNameFound && !closed)
        {
            CreateProfileSingleton input = CreateProfileSingleton.getSingleton();
            if(flag == true)
            {
                input.show("Username Taken");
            }
            else if(flag == false) {
                input.show("Create Profile");
            }
            flag = false;
            if (input.getSelection().equalsIgnoreCase("Close")) {
                input.closeDialog();
                closed = true;
            }
            if (input.getSelection().equals("Submit") && !closed) {
                File f = new File("Buzzword/resources/saved/");
                String absolutePath = f.getAbsolutePath();
                File folder = new File(absolutePath);
                File[] listOfFiles = folder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".json");
                    }
                });
                if (listOfFiles.length == 0) {
                    usr.createFile(input.getUserName(), input.getPassword());
                    switchToPostLoginScreen();
                    closed = true;
                }
                try {
                    JSONParser parser = new JSONParser();
                    Reader reader;
                    for (int i = 0; i < listOfFiles.length; i++) {
                        reader = new FileReader(listOfFiles[i].getAbsolutePath());
                        JSONObject obj = (JSONObject) parser.parse(reader);
                        String usrName = (String) obj.get("username");
                        if (input.getUserName().equalsIgnoreCase(usrName)) {
                            userNameFound = true;
                            flag = true;
                        }
                        else if((i == listOfFiles.length - 1) && (flag == false))
                        {
                            userNameFound = false;
                            usr.createFile(input.getUserName(), input.getPassword());
                            switchToPostLoginScreen();
                        }
                    }
                } catch (IOException p) {
                    p.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
    public static void handleGameCreationRequest(String cat, int level)
    {
        gameScreenGUI = new GameScreenGUI(cat, level);
        FirstSceneGUI.getGamePrimaryStage().setScene(gameScreenGUI.getScene());

        //set target score label to the target score based on the words in the generated grid
        int wordSize = 0;
        double multiplier = 0;
        if(cat.equalsIgnoreCase("three letter words"))
        {
            wordSize = 3;
        }
        else if(cat.equalsIgnoreCase("four letter words"))
        {
            wordSize = 4;
        }
        else if(cat.equalsIgnoreCase("five letter words"))
        {
            wordSize = 5;
        }
        switch(level)
        {
            case 1: //if the level to be generated is 1
                multiplier = .9; //user should find 10 % of words
                break;
            case 2:
                multiplier = .8; //user should find 20% of words
                break;
            case 3:
                multiplier = .7;
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                multiplier = .6;
                break;
        }
        int listSize = InGameController.getGrid().getWordsInBoard().size(); //size of the amount of words found by the solver
        int i = listSize * wordSize;
        double k = i - (i* multiplier);
        // Calculations done to get the target score as a multiple
        // of the points per word per category.
        targetScore = Math.round(( (int) k / wordSize)) * wordSize;
        if(targetScore== 0) //this case usually happens when there are 5 or less words found in the grid
        {
            targetScore+=wordSize; //just make them find one word
        }
        gameScreenGUI.setTargetLabel(targetScore);

    }
    public void setCategory(String cat)
    {
        category = cat;
    }
    public String getCategory()
    {
        return category;
    }
    void switchToPostLoginScreen()
    {
        PostLoginScreenGUI a = new PostLoginScreenGUI();
        FirstSceneGUI.getGamePrimaryStage().setScene(a.getScene());
    }

    public static GameScreenGUI getGameScreenGUI()
    {
        return gameScreenGUI;
    }

    public static int getTargetScore() {
        return targetScore;
    }
}