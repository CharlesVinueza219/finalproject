package controller;

import data.GameData;
import data.UserDataFile;
import grid.Grid;
import gui.CustomButton;
import gui.FirstSceneGUI;
import gui.GameScreenGUI;
import gui.PostLoginScreenGUI;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Charles Vinueza
 */
public class InGameController
{
    /**
     * For now these methods will do basic exiting. They eventually
     * should prompt the user if they want to exit will stopping
     * the timer and hiding the buttons
     */
    Timer timer;
    Integer timeRemaining;
    StringBuffer guessedWord;

    static Grid board;
    boolean paused;
    boolean wordDragged = false;
    int currentScore = 0;
    int currentLevel;
    boolean gameWon;
    boolean typingEnabled;
    boolean gameLost = false;
    char[][] boardCharArr;

    ArrayList<String> wordsGuessed = new ArrayList<String>();
    ArrayList<Character> wordTyped = new ArrayList<>();
    ArrayList<CustomButton> customButtons = new ArrayList<>();


    UserDataFile userData = FirstSceneGUI.getUserDataFile();
    GameData gameData = FirstSceneGUI.getGameData();
    public void handleLogoutRequest()
    {
        if(gameWon == true || gameLost == true) {
            YesNoCancelDialogSingleton close = YesNoCancelDialogSingleton.getSingleton();
            close.show("?", "Are you sure you want to logout?\nAll unsaved progress will be saved :)");
            if (close.getSelection().equalsIgnoreCase("yes")) {
                userData.saveData(gameData.getUsrName().toString());
                gameData.revertToNullUser();
                FirstSceneGUI.getGamePrimaryStage().setScene(FirstSceneGUI.getScene());
            } else {
                close.close();
            }
        }
        else {
            handlePauseRequest();
            BuzzwordController.gameScreenGUI.pause();
            YesNoCancelDialogSingleton close = YesNoCancelDialogSingleton.getSingleton();
            close.show("?", "Are you sure you want to logout?\nAll unsaved progress will be saved :)");
            if (close.getSelection().equalsIgnoreCase("yes")) {
                userData.saveData(gameData.getUsrName().toString());
                gameData.revertToNullUser();
                FirstSceneGUI.getGamePrimaryStage().setScene(FirstSceneGUI.getScene());
            } else {
                close.close();
            }
        }
    }
    public void handleHomeButtonRequest()
    {
        FirstSceneGUI.getGamePrimaryStage().setScene(PostLoginScreenGUI.getScene());
        timer.purge();
        timer.cancel();
    }
    public void handleExitRequest()
    {
        if(gameWon == true || gameLost == true) {
            YesNoCancelDialogSingleton close = YesNoCancelDialogSingleton.getSingleton();
            close.show("?", "Are you sure you want to exit?\nAll unsaved progress will be saved :)");
            if (close.getSelection().equalsIgnoreCase("yes")) {
                userData.saveData(gameData.getUsrName().toString());
                System.exit(0);
            } else {
                close.close();
            }
        }
        else {
            handlePauseRequest();
            BuzzwordController.gameScreenGUI.pause();
            YesNoCancelDialogSingleton close = YesNoCancelDialogSingleton.getSingleton();
            close.show("?", "Are you sure you want to exit?\nAll unsaved progress will be saved :)");
            if (close.getSelection().equalsIgnoreCase("yes")) {
                userData.saveData(gameData.getUsrName().toString());
                System.exit(0);
            } else {
                close.close();
            }
        }

    }
    public char[][] generateLetterGrid(String category, int level)
    {
        board = new Grid(category);
        String gameBoard = board.getGenerator().getBoard();

        boardCharArr = new char[4][4];

        boardCharArr[0][0] = gameBoard.charAt(0);
        boardCharArr[0][1] = gameBoard.charAt(1);;
        boardCharArr[0][2] = gameBoard.charAt(2);;
        boardCharArr[0][3] = gameBoard.charAt(3);
        boardCharArr[1][0] = gameBoard.charAt(4);
        boardCharArr[1][1] = gameBoard.charAt(5);
        boardCharArr[1][2] = gameBoard.charAt(6);
        boardCharArr[1][3] = gameBoard.charAt(7);
        boardCharArr[2][0] = gameBoard.charAt(8);
        boardCharArr[2][1] = gameBoard.charAt(9);
        boardCharArr[2][2] = gameBoard.charAt(10);
        boardCharArr[2][3] = gameBoard.charAt(11);
        boardCharArr[3][0] = gameBoard.charAt(12);
        boardCharArr[3][1] = gameBoard.charAt(13);
        boardCharArr[3][2] = gameBoard.charAt(14);
        boardCharArr[3][3] = gameBoard.charAt(15);

        return boardCharArr;
    }
    public void hideButtons()
    {
        BuzzwordController.gameScreenGUI.getGrid().setVisible(false);
    }
    public void showButtons()
    {
        BuzzwordController.gameScreenGUI.getGrid().setVisible(true);
    }
    public void handlePauseRequest()
    {
        paused = true;
        hideButtons();
        //change paused to true
        //stop timer
    }
    public void handlePlayRequest()
    {
        paused = false;
        showButtons();
        //change paused to true
        //stop timer
    }
    public void handleReplay()
    {
        setTypingEnabled(true);
        BuzzwordController.gameScreenGUI.enableDragging();
        timer.cancel();
        timer.purge();
        currentScore = 0;
        wordsGuessed.clear();
        gameWon = false;
        wordDragged = false;
        BuzzwordController.handleGameCreationRequest(BuzzwordController.gameScreenGUI.getCategory(), BuzzwordController.gameScreenGUI.getLevel());
    }
    public void playNextLevel()
    {
        timer.cancel();
        timer.purge();
        currentScore = 0;
        wordsGuessed.clear();
        gameWon = false;
        wordDragged = false;
        setTypingEnabled(true);
        BuzzwordController.gameScreenGUI.enableDragging();
        BuzzwordController.gameScreenGUI.getPlayNextLevelButton().setVisible(false);
        BuzzwordController.handleGameCreationRequest(BuzzwordController.gameScreenGUI.getCategory(), BuzzwordController.gameScreenGUI.getLevel() + 1);
    }
    public static Grid getGrid()
    {
        return board;
    }
    public void startButtonTimer(int time) {
        timeRemaining = time;
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!paused) {
                    Platform.runLater(new Runnable() {
                        public void run() {
                            GameScreenGUI.getTimeRemainingLabel().setText("Time Remaining " + timeRemaining.toString() + " seconds");
                            timeRemaining--;

                        }
                    });
                }
            }

        }, 0, 1000);
    }

    public void endButtonTimer()
    {
        timer.cancel();
        timer.purge();
    }
    public void play(String category, int level)
    {
        gameWon = false;
        String gameBoard = getGrid().getBoard().getBoard();
        switch(level)
        {
            case 1:
            case 2:
            case 3:
                startButtonTimer(150);
                break;
            case 4:
                startButtonTimer(140);
                break;
            case 5:
                startButtonTimer(130);
                break;
            case 6:
                startButtonTimer(120);
                break;
            case 7:
                startButtonTimer(110);
                break;
            case 8:
                startButtonTimer(100);
                break;
        }
        AnimationTimer timer = new AnimationTimer()
        {
            @Override

            public void handle(long now) {
                if(timeRemaining == -1 && gameWon == false)
                {
                    super.stop();
                    endButtonTimer();
                    AppMessageDialogSingleton loseMsg = AppMessageDialogSingleton.getSingleton();
                    loseMsg.show(":(", "Game lost");
                    gameLost = true;
                    setTypingEnabled(false);
                    BuzzwordController.gameScreenGUI.disableDragging();
                    showAllValidWords();
                    BuzzwordController.gameScreenGUI.addLossKeyEvents();
                }
                else
                {
                    if(wordDragged == true)
                    {
                        boolean correctWord = false;
                        if(isAlreadyGuessed(guessedWord.toString()) == true) {
                            correctWord = false;
                        }
                        else {
                            for (Object e : getGrid().getWordsInBoard()) {
                                if (guessedWord.toString().equalsIgnoreCase((String) e))
                                {
                                    correctWord = true;
                                }
                            }
                            if (correctWord) {
                                addCorrectGuess(guessedWord.toString());
                            } else {
                            }
                            wordDragged = false;
                        }
                    }
                    //
                    BuzzwordController.gameScreenGUI.getGameScreenScene().setOnKeyPressed((javafx.scene.input.KeyEvent event) ->
                    {
                        if(typingEnabled) {
                            BuzzwordController.gameScreenGUI.disableDragging();

                            if (event.isControlDown() && event.getCode() == KeyCode.Q) {
                                handleExitRequest();
                            }
                            else if(event.isControlDown() && event.getCode() == KeyCode.H) {
                                handleHomeButtonRequest();
                            }
                            else if(event.isControlDown() && event.getCode() == KeyCode.R) {
                                handleReplay();
                            }
                            else if(event.isShiftDown() && event.getCode() == KeyCode.RIGHT && BuzzwordController.gameScreenGUI.
                                    getPlayNextLevelButton().isVisible()) {
                                playNextLevel();
                            }
                            else if(!paused){
                                KeyCode keyInputted = event.getCode();
                                if (keyInputted.isLetterKey()) {
                                    char letter = keyInputted.toString().charAt(0);
                                    int counter = 0;
                                    if(gameBoard.contains(String.valueOf(letter)) && BuzzwordController.gameScreenGUI.getLetterList().isEmpty())
                                    {
                                        for(int i = 0; i<BuzzwordController.gameScreenGUI.getCustomButtons().size();i++)
                                        {
                                            CustomButton cstBtn = (CustomButton) BuzzwordController.gameScreenGUI.getCustomButtons().get(i);
                                            if(cstBtn.getLetter() == letter)
                                            {
                                                BuzzwordController.gameScreenGUI.pathColorUpdate(cstBtn.getBtn());
                                                if(counter == 0)
                                                {
                                                    wordTyped.add(letter);
                                                    BuzzwordController.gameScreenGUI.getLetterList().add(new GameScreenGUI.LetterUsed(keyInputted.toString().charAt(0)));
                                                }
                                                BuzzwordController.gameScreenGUI.getButtonList().add(cstBtn.getBtn());
                                                customButtons.add(cstBtn);
                                                counter++;
                                            }
                                        }
                                    }
                                    else
                                    {

                                        int count = 0;
                                        CustomButton adj;
                                        ArrayList<CustomButton> innerArrayList = new ArrayList<>();
                                        for(int i = 0; i< customButtons.size(); i++)
                                        {
                                            CustomButton c = customButtons.get(i);

                                            for(int q = 0; q< c.getAdjacentButtons().size(); q++)
                                            {
                                                if(c.getAdjacentButtons().get(q).getLetter() == Character.toUpperCase(letter))
                                                {
                                                    adj = c.getAdjacentButtons().get(q);
                                                    adj.addAdjancentButtons(BuzzwordController.gameScreenGUI.getGrid());
                                                    innerArrayList.add(adj);

                                                    if(count == 0) {
                                                        wordTyped.add(adj.getLetter());
                                                        BuzzwordController.gameScreenGUI.getLetterList().add(new GameScreenGUI.LetterUsed(adj.getLetter()));
                                                    }
                                                    BuzzwordController.gameScreenGUI.getButtonList().add(adj.getBtn());
                                                    BuzzwordController.gameScreenGUI.pathColorUpdate(adj.getBtn());
                                                    count++;
                                                }

                                            }
                                        }
                                        if(innerArrayList.isEmpty())
                                        {

                                        }
                                        else {
                                            customButtons.clear();
                                            customButtons.addAll(innerArrayList);
                                        }
                                        innerArrayList.clear();

                                    }
                                } else {
                                    if (keyInputted.equals(KeyCode.ENTER)) {
                                        boolean correctWord = false;
                                        setGuessedWordTyping(wordTyped);

                                        if(isAlreadyGuessed(guessedWord.toString()) == true) {
                                            correctWord = false;
                                        }
                                        else {
                                            for (Object e : getGrid().getWordsInBoard()) {
                                                if (guessedWord.toString().equalsIgnoreCase((String) e))
                                                {
                                                    correctWord = true;
                                                }
                                            }
                                            if (correctWord) {
                                                addCorrectGuess(guessedWord.toString());
                                            } else {
                                            }
                                            wordDragged = false;
                                        }
                                        wordTyped.clear();
                                        BuzzwordController.gameScreenGUI.getLetterList().clear();
                                        BuzzwordController.gameScreenGUI.enableDragging();
                                        BuzzwordController.gameScreenGUI.pathEnded(BuzzwordController.gameScreenGUI.getButtonList());
                                        BuzzwordController.gameScreenGUI.getButtonList().clear();
                                        customButtons.clear();
                                    }
                                }
                            }
                        }
                    });
                }
                if(currentScore >= BuzzwordController.getTargetScore() && !gameLost)
                {
                    currentLevel = BuzzwordController.gameScreenGUI.getLevel();
                    if(getGrid().getCategory().equalsIgnoreCase("three letter words"))
                    {
                        gameData.addLevelUnlocked("three letter words", currentLevel);
                    }
                    if(getGrid().getCategory().equalsIgnoreCase("four letter words"))
                    {
                        gameData.addLevelUnlocked("four letter words", currentLevel);
                    }
                    if(getGrid().getCategory().equalsIgnoreCase("five letter words"))
                    {
                        gameData.addLevelUnlocked("five letter words", currentLevel);
                    }

                    gameWon = true;

                    if(gameData.getGameStateSavable())
                    {
                        BuzzwordController.gameScreenGUI.getSaveButton().setVisible(true);
                        BuzzwordController.gameScreenGUI.addGameWonAndSavableKeyEvents();
                    }
                    else
                    {
                        BuzzwordController.gameScreenGUI.removeSaveKeyEvents();

                    }
                    if(BuzzwordController.gameScreenGUI.getLevel() != 8)
                    {
                        BuzzwordController.getGameScreenGUI().getPlayNextLevelButton().setVisible(true);
                    }
                    else
                    {
                        BuzzwordController.getGameScreenGUI().getPlayNextLevelButton().setVisible(false);
                    }
                    super.stop();
                    endButtonTimer();
                    AppMessageDialogSingleton winPopUp = AppMessageDialogSingleton.getSingleton();
                    winPopUp.show("!", "You win!");
                    setTypingEnabled(false);
                    BuzzwordController.gameScreenGUI.disableDragging();
                    showAllValidWords();
                    userData.saveData(gameData.getUsrName().toString());
                }
            }

            @Override
            public void stop() {
                super.stop();
            }
        };
        timer.start();
    }

    public boolean isAlreadyGuessed(String guessedWord)
    {
        if(wordsGuessed.isEmpty())
        {
            return false;
        }
        else
        {
            for(String e: wordsGuessed)
            {
                if(guessedWord.equalsIgnoreCase(e))
                {
                    return true;
                }
            }
            return false;
        }
    }

    public void addCorrectGuess(String guess)
    {
        String sum = "SUM:";
        int length = BuzzwordController.gameScreenGUI.getNames().size() * guess.length() + guess.length();
        String formattedSum = String.format("%-25s %3d", sum, length);
        BuzzwordController.gameScreenGUI.getSumLabel().setText(formattedSum);

        String formattedWord = String.format("%0$-31s %3d", guess, guess.length());
        BuzzwordController.gameScreenGUI.getNames().add(formattedWord);

        wordsGuessed.add(guess);

        currentScore+= guess.length();
        BuzzwordController.gameScreenGUI.getListView().scrollTo(wordsGuessed.size());

    }
    public void showAllValidWords()
    {
        for(String str: getGrid().getWordsInBoard())
        {
            if(isAlreadyGuessed(str))
            {

            }
            else
            {
                addCorrectGuess(str);
            }
        }
    }



    public void setGuessedWordDrag(ArrayList e)
    {
        guessedWord = new StringBuffer("");
        for(Object b: e)
        {
            guessedWord.append(b);
        }
        wordDragged = true;

    }
    public void setGuessedWordTyping(ArrayList e)
    {
        guessedWord = new StringBuffer("");
        for(Object b: e)
        {
            guessedWord.append(b);
        }

    }
    public void handleInGameSaveRequest()
    {
        userData.saveData(gameData.getUsrName().toString());
        gameData.setGamestateSavable(false);
        AppMessageDialogSingleton msgSave = AppMessageDialogSingleton.getSingleton();
        msgSave.show("!", "Game Saved");
        BuzzwordController.gameScreenGUI.getSaveButton().setVisible(false);
        BuzzwordController.gameScreenGUI.removeSaveKeyEvents();

    }
    public void setTypingEnabled(boolean a)
    {
        typingEnabled = a;
    }

}
