/**
 * IGNORE
 */
package controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Charlie on 11/18/16.
 */
public class TesterFolder {

    public static void main(String[] args) {

            String original = "Hello";
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            md.update(original.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            System.out.println("original:" + original);
            System.out.println("digested(hex):" + sb.toString());
        }
}