package data;

/**
 * @author Charles Vinueza
 */
public class GameData
{
    long threeLetterLevelsUnlocked;
    long fourLetterLevelsUnlocked;
    long fiveLetterLevelsUnlocked;
    StringBuffer usrName;
    static boolean GAMESTATE_SAVABLE = false;

    public GameData()
    {
        threeLetterLevelsUnlocked = 1;
        fourLetterLevelsUnlocked = 1;
        fiveLetterLevelsUnlocked = 1;
        usrName = new StringBuffer("");
        GAMESTATE_SAVABLE = false;
    }
    public void reset()
    {

    }
    public void setThreeLetterWordsUnlocked(long i)
    {
        this.threeLetterLevelsUnlocked = i;
    }
    public void setFourLetterWordsUnlocked(long j)
    {
        this.fourLetterLevelsUnlocked = j;
    }
    public void setFiveLetterWordsUnlocked(long k)
    {
        this.fiveLetterLevelsUnlocked = k;
    }

    public long getNumLevelsUnlocked(String category)
    {
        if(category.equalsIgnoreCase("three letter words"))
        {
            return threeLetterLevelsUnlocked;
        }
        else if(category.equalsIgnoreCase("four letter words"))
        {
            return fourLetterLevelsUnlocked;
        }
        else
        {
            return fiveLetterLevelsUnlocked;
        }
    }

    public void addLevelUnlocked(String category, int currentlevel)
    {
        if(category.equalsIgnoreCase("Three Letter Words"))
        {
            if(currentlevel < threeLetterLevelsUnlocked)
            {

            }
            else {
                if (threeLetterLevelsUnlocked == 8) {

                } else {
                    threeLetterLevelsUnlocked++;
                    GAMESTATE_SAVABLE = true;
                    //usr.saveData(usrName.toString());
                }
            }
        }
        if(category.equalsIgnoreCase("Four Letter Words"))
        {
            if(currentlevel < fourLetterLevelsUnlocked)
            {

            }
            else {
                if (fourLetterLevelsUnlocked == 8) {

                } else {
                    fourLetterLevelsUnlocked++;
                    GAMESTATE_SAVABLE = true;


                }
            }
        }
        if(category.equalsIgnoreCase("Five Letter Words"))
        {
            if(currentlevel < fiveLetterLevelsUnlocked)
            {

            }
            else {
                if (fiveLetterLevelsUnlocked == 8) {

                } else {
                    fiveLetterLevelsUnlocked++;
                    GAMESTATE_SAVABLE = true;
                }
            }
        }

    }
    public StringBuffer getUsrName()
    {
        return usrName;
    }
    public void setUsrName(String name)
    {
        usrName = new StringBuffer(name);
    }
    public void revertToNullUser()
    {
        threeLetterLevelsUnlocked = 1;
        fourLetterLevelsUnlocked = 1;
        fiveLetterLevelsUnlocked = 1;
        usrName = null;
    }
    public boolean getGameStateSavable()
    {
        return GAMESTATE_SAVABLE;
    }
    public void setGamestateSavable(boolean state)
    {
        GAMESTATE_SAVABLE = state;
    }
    public int getSumLevelsUnlockedGameData()
    {
        return (int) (threeLetterLevelsUnlocked + fourLetterLevelsUnlocked + fiveLetterLevelsUnlocked);
    }
}
