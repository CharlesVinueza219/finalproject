package data;

import com.fasterxml.jackson.databind.ObjectMapper;
import gui.FirstSceneGUI;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Charles Vinueza
 */
public class UserDataFile {
    JSONObject toSave;
    final String FILE_TYPE = ".json";
    private String currentPassword;
    int threeLevelsUnlockedInFile = 1;
    int fourLevelsUnlockedInFile = 1;
    int fiveLevelsUnlockedInFile = 1;

    public UserDataFile()
    {

    }
    public void createFile(String usrname, String pwd)
    {
        File file = new File("Buzzword/resources/saved/");
        String absolutePath = file.getAbsolutePath();
        String usrPath = absolutePath + "/" + usrname + FILE_TYPE;
        //create JSON File

        //convert typed password to an md5 hash and store in in the json file
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(pwd.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }

        toSave = new JSONObject();
        toSave.put("password", sb.toString());
        toSave.put("username", usrname);
        toSave.put("threeLetterLevelsUnlocked", 1);
        toSave.put("fourLetterLevelsUnlocked", 1);
        toSave.put("fiveLetterLevelsUnlocked", 1);

        FirstSceneGUI.getGameData().setUsrName(usrname);
        FirstSceneGUI.getGameData().setThreeLetterWordsUnlocked(1);
        FirstSceneGUI.getGameData().setFourLetterWordsUnlocked(1);
        FirstSceneGUI.getGameData().setFiveLetterWordsUnlocked(1);

        try {
            //Convert object to JSON string and save into file directly
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(usrPath), toSave);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void loadFileToGame(String path)
    {
       //File file = new File("Buzzword/resources/saved/");
        //String absolutePath = file.getAbsolutePath();
        //String usrPath = absolutePath + "/" + usrname + FILE_TYPE;
        JSONParser jsonParser = new JSONParser();
        //convert the necessary info to a JSON object (i.e. levels progressed)
        //with the exception of the username which will have to be passed into
        //GameData as gamedata.setUserName()
        try {
            Object obj = jsonParser.parse(new FileReader(path));
            JSONObject jsonObject = (JSONObject) obj;
            String userName = (String) jsonObject.get("username");
            currentPassword = (String) jsonObject.get("password");
            long threeLevels = (long) jsonObject.get("threeLetterLevelsUnlocked");
            long fourLevels = (long) jsonObject.get("fourLetterLevelsUnlocked");
            long fiveLevels = (long) jsonObject.get("fiveLetterLevelsUnlocked");
            threeLevelsUnlockedInFile = (int) threeLevels;
            fourLevelsUnlockedInFile = (int) threeLevels;
            fiveLevelsUnlockedInFile = (int) threeLevels;
            FirstSceneGUI.getGameData().setThreeLetterWordsUnlocked(threeLevels);
            FirstSceneGUI.getGameData().setFourLetterWordsUnlocked(fourLevels);
            FirstSceneGUI.getGameData().setFiveLetterWordsUnlocked(fiveLevels);
            FirstSceneGUI.getGameData().setUsrName(userName);
        }
        catch (Exception  e)
        {
            e.printStackTrace();
        }
    }
    public void saveData(String path)
    {
        File file = new File("Buzzword/resources/saved/");
        String absolutePath = file.getAbsolutePath();
        String usrPath = absolutePath + "/" + path + FILE_TYPE;

        JSONParser jsonParser = new JSONParser();

        toSave = new JSONObject();
        toSave.put("password", currentPassword);
        toSave.put("username", FirstSceneGUI.getGameData().getUsrName().toString());
        toSave.put("threeLetterLevelsUnlocked", FirstSceneGUI.getGameData().getNumLevelsUnlocked("Three Letter Words"));
        toSave.put("fourLetterLevelsUnlocked", FirstSceneGUI.getGameData().getNumLevelsUnlocked("Four Letter Words"));
        toSave.put("fiveLetterLevelsUnlocked", FirstSceneGUI.getGameData().getNumLevelsUnlocked("Five Letter Words"));

        threeLevelsUnlockedInFile = (int) FirstSceneGUI.getGameData().getNumLevelsUnlocked("Three Letter Words");
        fourLevelsUnlockedInFile = (int) FirstSceneGUI.getGameData().getNumLevelsUnlocked("Four Letter Words");
        fiveLevelsUnlockedInFile = (int) FirstSceneGUI.getGameData().getNumLevelsUnlocked("Five Letter Words");

        try {
            //Convert object to JSON string and save into file directly
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(usrPath), toSave);

        } catch (IOException e) {
            e.printStackTrace();
        }
        FirstSceneGUI.getGameData().setGamestateSavable(false);


    }

    public int getLevelsUnlockedCategory(String category) {
        if (category.equalsIgnoreCase("three letter words")) {
            return threeLevelsUnlockedInFile;
        } else if (category.equalsIgnoreCase("four letter words")) {
            return fourLevelsUnlockedInFile;
        } else {
            return fiveLevelsUnlockedInFile;
        }
    }
    public int getSumLevelsUnlocked()
    {
        return threeLevelsUnlockedInFile + fourLevelsUnlockedInFile + fiveLevelsUnlockedInFile;
    }
    public void setCurrentPassword(String s)
    {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(s.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        currentPassword = sb.toString();
        saveData(FirstSceneGUI.getGameData().getUsrName().toString());
    }


}
