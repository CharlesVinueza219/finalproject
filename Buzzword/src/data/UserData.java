package data;

import controller.BuzzwordController;

/**
 * @author Charles Vinueza
 */
public class UserData {
    BuzzwordController bz = new BuzzwordController();

    private int threeLetterLevelsUnlocked;
    private int fourLetterLevelsUnlocked;
    private int fiveLetterLevelsUnlocked;

    public UserData()
    {
        threeLetterLevelsUnlocked = 1;
        fourLetterLevelsUnlocked = 1;
        fiveLetterLevelsUnlocked = 1;
    }



}
