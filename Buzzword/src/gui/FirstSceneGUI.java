package gui;

import controller.BuzzwordController;
import data.GameData;
import data.UserDataFile;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.*;

import static javafx.scene.paint.Color.rgb;

/**
 * @author Charles Vinueza
 */

public class FirstSceneGUI extends Application
{
        BuzzwordController controller = new BuzzwordController();
        DropShadow shadow = new DropShadow();
        static Scene loginCreateProfileScene;
        static Stage gamePrimaryStage;
        public static GameData gm = new GameData();
        public static UserDataFile usr = new UserDataFile();

    @Override
    public void start(Stage primaryStage) throws Exception
    {

        InputDialogSingleton input = InputDialogSingleton.getSingleton();
        input.init(gamePrimaryStage);
        AppMessageDialogSingleton app = AppMessageDialogSingleton.getSingleton();
        app.init(gamePrimaryStage);
        YesNoCancelDialogSingleton yesNoCancel = YesNoCancelDialogSingleton.getSingleton();
        yesNoCancel.init(gamePrimaryStage);
        CreateProfileSingleton create = CreateProfileSingleton.getSingleton();
        create.init(gamePrimaryStage);
        SetPasswordSingleton sp = SetPasswordSingleton.getSingleton();
        sp.init(gamePrimaryStage);

        gamePrimaryStage = new Stage();
        initScene();
        gamePrimaryStage.setScene(loginCreateProfileScene);
        gamePrimaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage = gamePrimaryStage;
        primaryStage.show();


    }
    public static void main(String[] args)
    {
        launch(args);
    }

    public void initScene()
    {
        BorderPane borderPane = new BorderPane();
        gamePrimaryStage.setTitle("Buzzword");
        Button loginButton = new Button();
        loginButton.setText("Login");
        loginButton.setPrefWidth(175);
        loginButton.setPrefHeight(40);
        loginButton.setOnMouseClicked(e -> {
                controller.handleLoginRequest();

        });
        loginButton.setStyle("-fx-background-color:  #9eb8e0" );

        loginButton.setOnMouseEntered(e -> loginButton.setEffect(shadow));
        loginButton.setOnMouseExited(e -> loginButton.setEffect(null));

        Button createNewProfileButton = new Button();
        createNewProfileButton.setText("Create New Profile");
        createNewProfileButton.setPrefWidth(175);
        createNewProfileButton.setPrefHeight(40);
        createNewProfileButton.setOnMouseClicked(event -> controller.handleCreateProfileRequest());
        createNewProfileButton.setStyle("-fx-background-color:  #9eb8e0" );
        createNewProfileButton.setOnMouseEntered(e -> createNewProfileButton.setEffect(shadow));
        createNewProfileButton.setOnMouseExited(e -> createNewProfileButton.setEffect(null));

        Button helpButton = new Button();
        helpButton.setText("Need Help?");
        helpButton.setPrefWidth(175);
        helpButton.setPrefHeight(40);
        helpButton.setOnMouseClicked(event -> controller.handleHelpRequest());
        helpButton.setStyle("-fx-background-color:  #9eb8e0" );
        helpButton.setOnMouseEntered(e -> helpButton.setEffect(shadow));
        helpButton.setOnMouseExited(e -> helpButton.setEffect(null));


        VBox toolbarBox = new VBox();
        toolbarBox.setPrefWidth(175);
        toolbarBox.setPadding(new Insets(225, 0, 0, 0));
        toolbarBox.setMargin(loginButton, new Insets(0, 0, 25, 0)); //This is where you should be looking at.
        toolbarBox.setMargin(createNewProfileButton, new Insets(0, 0, 25, 0)); //This is where you should be looking at.


        toolbarBox.getChildren().addAll(loginButton, createNewProfileButton, helpButton);
        borderPane.setLeft(toolbarBox);
        borderPane.getLeft().setStyle("-fx-background-color: #c5d4ed;");

        VBox main = new VBox(); // main pain next to toolbar

        HBox closeButtonBox = new HBox();
        Image imageOk = new Image("images/CloseCross.png");
        ImageView btnImage = new ImageView(imageOk);
        btnImage.setFitHeight(20);
        btnImage.setFitWidth(20);


        Button closeButton = new Button("", btnImage);
        closeButton.setStyle("-fx-background-color: transparent;");
        closeButtonBox.getChildren().add(closeButton);

        closeButton.setOnMouseEntered(e -> closeButton.setEffect(shadow));
        closeButton.setOnMouseExited(e -> closeButton.setEffect(null));
        closeButton.setOnMouseClicked(e -> controller.handleExitRequest());


        HBox titleBox = new HBox();
        Text title = new Text();
        title.setFont(new Font("Times New Roman", 50));
        title.setText("!!Buzzword!!");
        title.setFill(rgb(197,212,237));
        titleBox.getChildren().addAll(title);


        main.setStyle("-fx-background-color:  #779bd4");
        main.getChildren().add(closeButtonBox);
        main.getChildren().add(titleBox);


        titleBox.setAlignment(Pos.CENTER);
        closeButtonBox.setAlignment(Pos.TOP_RIGHT);

        borderPane.setCenter(main);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.setHgap(50);
        grid.setVgap(50);

        String letter;
        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                if(r == 0 & c== 0)
                {
                    letter = "B";
                }
                else if(r == 0 & c== 1)
                {
                    letter = "U";
                }
                else if(r == 1 & c== 0)
                {
                    letter = "Z";
                }
                else if(r == 1 & c== 1)
                {
                    letter = "Z";
                }
                else if(r == 2 & c== 2)
                {
                    letter = "W";
                }
                else if(r == 2 & c== 3)
                {
                    letter = "O";
                }
                else if(r == 3 & c== 2)
                {
                    letter = "R";
                }
                else if(r == 3 & c== 3)
                {
                    letter = "D";
                }
                else
                {
                    letter = "";
                }
                Button button = new Button(letter);
                button.setStyle(
                        "-fx-background-radius: 75em; " +
                                "-fx-min-width: 75px; " +
                                "-fx-min-height: 75px; " +
                                "-fx-max-width: 75px; " +
                                "-fx-max-height: 75px;" +
                                "-fx-background-color: #c5d4ed; " +
                                "-fx-font: 22 Times_New_Roman;"

                );
                //315a9b
                grid.add(button, c, r);
            }
        }
        grid.setPadding(new Insets(40, 0, 0, 0));
        grid.setAlignment(Pos.CENTER);
        main.getChildren().add(grid);

        loginCreateProfileScene = new Scene(borderPane, 900, 650);


        addKeyEvents();


    }

    public static Scene getScene()
    {
        return loginCreateProfileScene;
    }
    public static Stage getGamePrimaryStage()
    {
        return gamePrimaryStage;
    }

    public static GameData getGameData()
    {
        return gm;
    }
    public static UserDataFile getUserDataFile()
    {
        return usr;
    }

    public void addKeyEvents()
    {
        loginCreateProfileScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.L && event.isControlDown())
            {
                controller.handleLoginRequest();
            }
            if(event.getCode() == KeyCode.P && event.isControlDown() && event.isShiftDown())
            {
                controller.handleCreateProfileRequest();
            }
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                controller.handleExitRequest();
            }
        });
    }


}

