package gui;

import controller.BuzzwordController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import static javafx.scene.paint.Color.rgb;

/**
 * @author Charles Vinueza
 */

public class PostLoginScreenGUI
{
    static Scene postLoginScreenScene;
    BuzzwordController controller = new BuzzwordController();
    DropShadow shadow = new DropShadow();
    ChoiceBox<String> choiceBox;
    String accountChoice;
    public PostLoginScreenGUI()
    {
        initPostLoginScreenScene();
    }
    public static Scene getScene()
    {
        return postLoginScreenScene;
    }
    public void initPostLoginScreenScene()
    {
        BorderPane borderPane = new BorderPane();


        /*Image backarrow = new Image("images/backarrow.png");
        ImageView logoutImg = new ImageView(backarrow);
        logoutImg.setFitHeight(15);
        logoutImg.setFitWidth(15);*/
        String currentUserName = FirstSceneGUI.getGameData().getUsrName().toString();
        final ChoiceBox editLogoutChoiceBox = new ChoiceBox(FXCollections.observableArrayList(currentUserName, "Logout", "Edit Account Password"));

        editLogoutChoiceBox.setStyle("-fx-background-color: #9eb8e0;" + " -fx-text-origin: center;");
        editLogoutChoiceBox.setPrefWidth(175);
        editLogoutChoiceBox.setPrefHeight(40);
        editLogoutChoiceBox.setValue(currentUserName);

        final String[] choices = new String[]{currentUserName, "Logout", "Edit Account Password"};
        editLogoutChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                accountChoice = choices[newValue.intValue()];
                if(accountChoice.equals("Logout"))
                {
                    controller.handleLogoutRequest();
                }
                else if(accountChoice.equals("Edit Account Password"))
                {
                    controller.handlePasswordChangeRequest();
                }
                else
                {

                }
            }});


       /* Button profileButton = new Button();
        profileButton.setText(FirstSceneGUI.getGameData().getUsrName().toString());
        profileButton.setPrefWidth(175);
        profileButton.setPrefHeight(40);
        profileButton.setOnMouseClicked(e -> controller.handleLogoutRequest());
        profileButton.setStyle("-fx-background-color:  #9eb8e0" );

        profileButton.setOnMouseEntered(e -> profileButton.setEffect(shadow));
        profileButton.setOnMouseExited(e -> profileButton.setEffect(null));*/

        choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("Select Game Mode");
        choiceBox.getItems().add("Three Letter Words");
        choiceBox.getItems().add("Four Letter Words");
        choiceBox.getItems().add("Five Letter Words");
        choiceBox.setValue("Select Game Mode");
        choiceBox.setPrefWidth(175);
        choiceBox.setPrefHeight(40);
        choiceBox.setStyle("-fx-background-color: #9eb8e0;" + " -fx-text-origin: center;");

        Button startPlayingButton = new Button();
        startPlayingButton.setText("Start Playing");
        startPlayingButton.setPrefWidth(175);
        startPlayingButton.setPrefHeight(40);
        startPlayingButton.setOnMouseClicked(e ->
        {
            String choice = choiceBox.getSelectionModel().getSelectedItem().toString();
            LevelSelectScreenGUI levelSelectScreenGUI = new LevelSelectScreenGUI(choice, FirstSceneGUI.getGameData().getNumLevelsUnlocked(choice));
            controller.handleLevelSelectRequest(choice);
        });
        startPlayingButton.setStyle("-fx-background-color:  #9eb8e0" );
        startPlayingButton.setOnMouseEntered(e -> startPlayingButton.setEffect(shadow));
        startPlayingButton.setOnMouseExited(e -> startPlayingButton.setEffect(null));

        VBox toolbarBox = new VBox();
        toolbarBox.setPrefWidth(175);
        toolbarBox.setPadding(new Insets(220, 0, 0, 0));
        toolbarBox.setMargin(editLogoutChoiceBox, new Insets(0, 0, 25, 0)); //This is where you should be looking at.

        toolbarBox.setMargin(choiceBox, new Insets(0, 0, 25, 0)); //This is where you should be looking at.

        toolbarBox.getChildren().addAll(editLogoutChoiceBox, choiceBox, startPlayingButton);

        borderPane.setLeft(toolbarBox);
        borderPane.getLeft().setStyle("-fx-background-color: #c5d4ed;");

        VBox main = new VBox(); // main pain next to toolbar

        HBox closeButtonBox = new HBox();
        Image imageOk = new Image("images/CloseCross.png");
        ImageView btnImage = new ImageView(imageOk);
        btnImage.setFitHeight(20);
        btnImage.setFitWidth(20);


        Button closeButton = new Button("", btnImage);
        closeButton.setStyle("-fx-background-color: transparent;");
        closeButtonBox.getChildren().add(closeButton);

        closeButton.setOnMouseEntered(e -> closeButton.setEffect(shadow));
        closeButton.setOnMouseExited(e -> closeButton.setEffect(null));
        closeButton.setOnMouseClicked(e -> controller.handleExitRequest());


        HBox titleBox = new HBox();
        Text title = new Text();
        title.setFont(new Font("Times New Roman", 50));
        title.setText("!!Buzzword!!");
        title.setFill(rgb(197,212,237));
        titleBox.getChildren().addAll(title);


        main.setStyle("-fx-background-color:  #779bd4");
        main.getChildren().add(closeButtonBox);
        main.getChildren().add(titleBox);


        titleBox.setAlignment(Pos.CENTER);
        closeButtonBox.setAlignment(Pos.TOP_RIGHT);

        borderPane.setCenter(main);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.setHgap(50);
        grid.setVgap(50);

        String letter;
        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                if(r == 0 & c== 0)
                {
                    letter = "B";
                }
                else if(r == 0 & c== 1)
                {
                    letter = "U";
                }
                else if(r == 1 & c== 0)
                {
                    letter = "Z";
                }
                else if(r == 1 & c== 1)
                {
                    letter = "Z";
                }
                else if(r == 2 & c== 2)
                {
                    letter = "W";
                }
                else if(r == 2 & c== 3)
                {
                    letter = "O";
                }
                else if(r == 3 & c== 2)
                {
                    letter = "R";
                }
                else if(r == 3 & c== 3)
                {
                    letter = "D";
                }
                else
                {
                    letter = "";
                }
                Button button = new Button(letter);
                button.setStyle(
                        "-fx-background-radius: 75em; " +
                                "-fx-min-width: 75px; " +
                                "-fx-min-height: 75px; " +
                                "-fx-max-width: 75px; " +
                                "-fx-max-height: 75px;" +
                                "-fx-background-color: #c5d4ed; " +
                                "-fx-font: 22 Times_New_Roman;"

                ); //315a9b
                grid.add(button, c, r);
            }
        }
        grid.setPadding(new Insets(40, 0, 0, 0));
        grid.setAlignment(Pos.CENTER);
        main.getChildren().add(grid);


        postLoginScreenScene = new Scene(borderPane, 900, 650);
        addKeyEvents();
    }
    public void addKeyEvents()
    {
        postLoginScreenScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.P && event.isControlDown())
            {
                String choice = choiceBox.getSelectionModel().getSelectedItem().toString();
                LevelSelectScreenGUI levelSelectScreenGUI = new LevelSelectScreenGUI(choice, FirstSceneGUI.getGameData().getNumLevelsUnlocked(choice));
                controller.handleLevelSelectRequest(choice);
            }
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                controller.handleExitRequest();
            }
            if(event.getCode() == KeyCode.L && event.isControlDown())
            {
                controller.handleLogoutRequest();
            }
        });
    }

}
