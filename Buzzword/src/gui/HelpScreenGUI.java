package gui;

import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import static javafx.scene.paint.Color.rgb;

/**
 * Created by Charlie on 11/28/16.
 */
public class HelpScreenGUI {
    DropShadow shadow = new DropShadow();
    static Scene helpScreenScene;
    BuzzwordController controller = new BuzzwordController();

    public HelpScreenGUI()
    {
        displayHelpScene();
    }
    public static Scene getScene()
    {
        return helpScreenScene;
    }
    public void displayHelpScene()
    {
        BorderPane borderPane = new BorderPane();

        Button homeButton = new Button();
        homeButton.setText("Back");
        homeButton.setPrefWidth(175);
        homeButton.setPrefHeight(40);
        homeButton.setOnMouseClicked(event -> controller.helpReturnButtonRequest());
        homeButton.setStyle("-fx-background-color:  #9eb8e0" );
        homeButton.setOnMouseEntered(e -> homeButton.setEffect(shadow));
        homeButton.setOnMouseExited(e -> homeButton.setEffect(null));

        VBox toolbarBox = new VBox();
        toolbarBox.setPrefWidth(175);
        toolbarBox.setPadding(new Insets(280, 0, 0, 0));
        toolbarBox.getChildren().add(homeButton);

        borderPane.setLeft(toolbarBox);
        borderPane.getLeft().setStyle("-fx-background-color: #c5d4ed;");

        VBox main = new VBox(); // main pain next to toolbar

        HBox closeButtonBox = new HBox();
        Image imageOk = new Image("images/CloseCross.png");
        ImageView btnImage = new ImageView(imageOk);
        btnImage.setFitHeight(20);
        btnImage.setFitWidth(20);

        Button closeButton = new Button("", btnImage);
        closeButton.setStyle("-fx-background-color: transparent;");
        closeButtonBox.getChildren().add(closeButton);

        closeButton.setOnMouseEntered(e -> closeButton.setEffect(shadow));
        closeButton.setOnMouseExited(e -> closeButton.setEffect(null));
        closeButton.setOnMouseClicked(e -> controller.handleExitRequest());


        HBox titleBox = new HBox();
        Text title = new Text();
        title.setFont(new Font("Times New Roman", 50));
        title.setText("Buzzword Help");
        title.setFill(rgb(197,212,237));
        titleBox.getChildren().addAll(title);


        main.setStyle("-fx-background-color:  #779bd4");
        main.getChildren().addAll(closeButtonBox, titleBox);

        titleBox.setAlignment(Pos.CENTER);
        closeButtonBox.setAlignment(Pos.TOP_RIGHT);

        borderPane.setCenter(main);

        ScrollPane scrollPane = new ScrollPane();
        GridPane textGrid = new GridPane();
        textGrid.setPadding(new Insets(5, 5, 5, 5));
        textGrid.setHgap(20);
        textGrid.setVgap(20);

        Label loginHeading = new Label();
        loginHeading.setFont(new Font("Times New Roman", 20));
        //loginHeading.setTextFill(rgb(99, 139, 207));
        loginHeading.setText("Login/Logout Screen");

        Label loginBody = new Label();
        loginBody.setText("If you are a new user create a profile, specify a username and password. Remember them to log in the next \ntime." +
                          "\n\nIf you are a returning user, login with the credentials you were asked to remember\n" +
                          "Shortcuts:\tCtrl + L : Login\n\t\t\tCtrl + Shift + P : Create New Profile\n\t\t\tCtrl + Q : Quit Application");
        loginBody.setTextFill(rgb(197,212,237));

        textGrid.add(loginHeading, 1, 0);
        textGrid.add(loginBody, 1, 1);


        Label postLoginHomeScreenHeading = new Label();
        postLoginHomeScreenHeading.setText("Post-login Screen/ Home Screen");
        postLoginHomeScreenHeading.setFont(new Font("Times New Roman", 20));
        //postLoginHomeScreenHeading.setTextFill(rgb(99, 139, 207));

        Label postLoginHomeScreenBody = new Label();
        postLoginHomeScreenBody.setText("Clicking on the button with your username will log you out of the game. This is a button that will remain on\nt" +
                "the in-game screen as well as in the level selection screen. More information on those can be found below.\n\n"
                + "The button under it is a drop down menu that allows you to select your game mode\n\nWhen one of the three game modes is selected in this" +
                " button, clicking the start playing button will bring you\nto the level selection screen\n" +
                "Shortcuts:\tCtrl + P : Start Playing\n\t\t\tCtrl + L : Logout\n\t\t\tCtrl + Q : Quit Application");
        postLoginHomeScreenBody.setTextFill(rgb(197,212,237));

        textGrid.add(postLoginHomeScreenHeading, 1, 2);
        textGrid.add(postLoginHomeScreenBody, 1, 3);

        Label levelSelectionScreenHead = new Label();
        levelSelectionScreenHead.setText("Level Selection Screen");
        levelSelectionScreenHead.setFont(new Font("Times New Roman", 20));
        //levelSelectionScreenHead.setTextFill(rgb(99, 139, 207));

        Label levelSelectionScreenBody = new Label();
        levelSelectionScreenBody.setText("Each category has eight levels, displayed by the eight buttons. When you start playing for " +
                "the first\ntime, you will only be able to play level one for each category. After you complete each level you unlock\n" +
                "the next one in that specific category. Complete all eight levels in each category to become a master\nof the game.\n" +
                "Shortcuts:\tCtrl + H : Return to Home Screen\n\t\t\tCtrl + L : Logout\n\t\t\tCtrl + Q : Quit Application");
        levelSelectionScreenBody.setTextFill(rgb(197,212,237));

        textGrid.add(levelSelectionScreenHead, 1, 4);
        textGrid.add(levelSelectionScreenBody, 1, 5);

        Label gameplayHead = new Label();
        gameplayHead.setText("Gameplay Screen");
        gameplayHead.setFont(new Font("Times New Roman", 20));
       // gameplayHead.setTextFill(rgb(99, 139, 207));

        Label gameplayBody = new Label();
        gameplayBody.setText("In each game you play there will be a target score you must reach in order to complete the level and unlock\nthe next level in the category. For the first 3 levels you will have two and a half minutes to reach the target\nscore. Starting from level four, you will have 10 less seconds to reach the target score.\n" +
                "\n" +
                "Percentage of total words in the grid needed to be found per level\n" +
                "\tLevel 1: 10%\n" +
                "\tLevel 2: 20%\n" +
                "\tLevel 3: 30%\n" +
                "\tLevels 4-8: 40%\n" +
                "\n" +
                "As you discover each word, by either typing the word out and pressing enter or by dragging over the letters\nwith your mouse, your total score will update on the right side along with the list of words you’ve already\nfound. You cannot use the same words twice.\n" +
                "\n" +
                "Once you reach the target score you have the option of starting the next level and saving your progress.\nShould you lose the level you can replay it. Buttons will appear on screen when these things occur.\n" +
                "\nShortcuts:\tCtrl + H : Return to Home Screen\n\t\t\tCtrl + L : Logout\n\t\t\tCtrl + Q : Quit Application\n\t\t\tCtrl + R : Replay Level\n\t\t\t" +
                "Ctrl + S : Save Game\n\t\t\tShift + > : Start Next Level");
        gameplayBody.setTextFill(rgb(197,212,237));

        textGrid.add(gameplayHead, 1, 6);
        textGrid.add(gameplayBody, 1, 7);

        /*Label postLoginHomeScreenHeading = new Label();
        postLoginHomeScreenHeading.setText("Post-login Screen/ Home Screen");
        Label postLoginHomeScreenBody = new Label();
        postLoginHomeScreenBody.setText("Clicking on the button with your username will log you out of the game. This is a button that will remain on " +
                "the\n in-game screen as well as in the level selection screen. More information on those can be found below.");

        textGrid.add(postLoginHomeScreenHeading, 1, 4);
        textGrid.add(postLoginHomeScreenBody, 1, 5);*/

        scrollPane.setContent(textGrid);
        scrollPane.setStyle("-fx-background: #779bd4");
        scrollPane.setPrefHeight(700);
        scrollPane.setPrefWidth(600);
        main.getChildren().addAll(scrollPane);

        helpScreenScene = new Scene(borderPane, 900, 650);
    }
}
