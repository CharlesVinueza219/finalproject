package gui;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charlie on 12/1/16.
 */
public class CustomButton {

    Button btn;
    int rowPos;
    int colPos;
    char letter;
    List<CustomButton> adjacentButtons;
    int count = 0;

    public CustomButton(Button initButton, int rowP, int colP, char ltr)
    {
        btn = initButton;
        rowPos = rowP;
        colPos = colP;
        letter = ltr;
    }

    public Button getBtn() {
        return btn;
    }

    public char getLetter() {
        return letter;
    }

    public int getColPos() {
        return colPos;
    }

    public int getRowPos() {
        return rowPos;
    }

    public void addAdjancentButtons(GridPane grid)
    {
        //Get Button at upper left corner of button
        //make sure that calculation isn't out of the bounds 0-3
        //cast that button to a CustomButton and add it to the adjacentButtons list
        adjacentButtons = new ArrayList();
        addToList(rowPos-1, colPos-1, grid);
        addToList(rowPos-1, colPos, grid);
        addToList(rowPos-1, colPos+1, grid);
        addToList(rowPos, colPos-1, grid);
        addToList(rowPos, colPos+1, grid);
        addToList(rowPos+1, colPos-1, grid);
        addToList(rowPos+1, colPos, grid);
        addToList(rowPos+1, colPos+1, grid);

    }
    public Node getNodeByRowColumnIndex (final int row, final int column, GridPane gridPane) {
        Node result = null;
        ObservableList<Node> childrens = gridPane.getChildren();

        for (Node node : childrens) {
            if(gridPane.getRowIndex(node) == row && gridPane.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }

        return result;
    }
    public void addToList(int rowPosToAdd, int colPosToAdd, GridPane grid)
    {
        Button cst = (Button) getNodeByRowColumnIndex(rowPosToAdd, colPosToAdd, grid);
        if(cst != null) {
            CustomButton cstBtn = new CustomButton(cst, rowPosToAdd, colPosToAdd, cst.getText().charAt(0));
            if (cstBtn != null) {
                adjacentButtons.add(cstBtn);
                count++;
            }
        }
    }
    public boolean isAdjacentTo(char letter)
    {
        boolean toReturn = false;
        for(int i = 0; i<adjacentButtons.size();i++)
        {
            if(adjacentButtons.get(i).getLetter() == Character.toUpperCase(letter))
            {
                toReturn = true;
            }
        }
        return toReturn;
    }

    public List<CustomButton> getAdjacentButtons() {
        return adjacentButtons;
    }
}
