package gui;

import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import static javafx.scene.paint.Color.rgb;

/**
 * @author Charles Vinueza
 */

public class LevelSelectScreenGUI
{
        static Scene levelSelectScreenScene;
        BuzzwordController controller = new BuzzwordController();
        DropShadow shadow = new DropShadow();

        public LevelSelectScreenGUI(String category, long numLevels)
        {
            initLevelSelectScreenScene(category, numLevels);
        }
        public static Scene getScene()
        {
            return levelSelectScreenScene;
        }
        public void initLevelSelectScreenScene(String s, long numLevels)
        {
            BorderPane borderPane = new BorderPane();

            Image backarrow = new Image("images/backarrow.png");
            ImageView logoutImg = new ImageView(backarrow);
            logoutImg.setFitHeight(15);
            logoutImg.setFitWidth(15);
            Button profileButton = new Button(FirstSceneGUI.getGameData().getUsrName().toString(), logoutImg);

            profileButton.setPrefWidth(175);
            profileButton.setPrefHeight(40);
            profileButton.setOnMouseClicked(e -> controller.handleLogoutRequest());
            profileButton.setStyle("-fx-background-color:  #9eb8e0" );
            profileButton.setOnMouseEntered(e -> profileButton.setEffect(shadow));
            profileButton.setOnMouseExited(e -> profileButton.setEffect(null));


            Button homeButton = new Button();
            homeButton.setText("Home");
            homeButton.setPrefWidth(175);
            homeButton.setPrefHeight(40);
            homeButton.setOnMouseClicked(event -> controller.handleHomeButtonRequest());
            homeButton.setStyle("-fx-background-color:  #9eb8e0" );
            homeButton.setOnMouseEntered(e -> homeButton.setEffect(shadow));
            homeButton.setOnMouseExited(e -> homeButton.setEffect(null));

            VBox toolbarBox = new VBox();
            toolbarBox.setPrefWidth(175);
            toolbarBox.setPadding(new Insets(235, 0, 0, 0));
            toolbarBox.setMargin(profileButton, new Insets(0, 0, 25, 0)); //This is where you should be looking at.

            toolbarBox.getChildren().add(profileButton);
            toolbarBox.getChildren().add(homeButton);

            borderPane.setLeft(toolbarBox);
            borderPane.getLeft().setStyle("-fx-background-color: #c5d4ed;");

            VBox main = new VBox(); // main pain next to toolbar

            HBox closeButtonBox = new HBox();
            Image imageOk = new Image("images/CloseCross.png");
            ImageView btnImage = new ImageView(imageOk);
            btnImage.setFitHeight(20);
            btnImage.setFitWidth(20);


            Button closeButton = new Button("", btnImage);
            closeButton.setStyle("-fx-background-color: transparent;");
            closeButtonBox.getChildren().add(closeButton);

            closeButton.setOnMouseEntered(e -> closeButton.setEffect(shadow));
            closeButton.setOnMouseExited(e -> closeButton.setEffect(null));
            closeButton.setOnMouseClicked(e -> controller.handleExitRequest());


            HBox titleBox = new HBox();
            Text title = new Text();
            title.setFont(new Font("Times New Roman", 50));
            title.setText("!!Buzzword!!");
            title.setFill(rgb(197,212,237));
            titleBox.getChildren().addAll(title);

            HBox categoryTitleBox = new HBox();
            Text categoryTitle = new Text();
            categoryTitle.setFont(new Font("Times New Roman", 50));

            categoryTitle.setText(s);
            categoryTitle.setUnderline(true);
            categoryTitle.setFill(rgb(235,240,249));
            categoryTitleBox.getChildren().addAll(categoryTitle);
            categoryTitleBox.setPadding(new Insets(30, 0, 0, 0));

            main.setStyle("-fx-background-color:  #779bd4");
            main.getChildren().add(closeButtonBox);
            main.getChildren().add(titleBox);
            main.getChildren().add(categoryTitleBox);


            titleBox.setAlignment(Pos.CENTER);
            closeButtonBox.setAlignment(Pos.TOP_RIGHT);
            categoryTitleBox.setAlignment(Pos.CENTER);

            borderPane.setCenter(main);

            GridPane grid = new GridPane();
            grid.setPadding(new Insets(5, 5, 5, 5));
            grid.setHgap(50);
            grid.setVgap(50);

            int num = 1;
            for (int r = 0; r < 2; r++) {
                for (int c = 0; c < 4; c++) {
                    Button button = new Button(String.valueOf(num));
                    if(num > numLevels)
                    {
                        button.setDisable(true);
                        button.setStyle("-fx-background-color: #bfbfbf;");
                    }
                        num++;
                        button.setOnMouseClicked(e -> controller.handleGameCreationRequest(s, Integer.valueOf(button.getText())));
                        button.setStyle(
                                "-fx-background-radius: 75em; " +
                                        "-fx-min-width: 75px; " +
                                        "-fx-min-height: 75px; " +
                                        "-fx-max-width: 75px; " +
                                        "-fx-max-height: 75px;" +
                                        "-fx-background-color: #c5d4ed; " +
                                        "-fx-font: 22 Times_New_Roman;"

                        ); //315a9b
                        grid.add(button, c, r);

                }
            }

            grid.setPadding(new Insets(40, 0, 0, 0));
            grid.setAlignment(Pos.CENTER);
            main.getChildren().add(grid);


            levelSelectScreenScene = new Scene(borderPane, 900, 650);
            addKeyEvents();
        }
    public void addKeyEvents()
    {
        levelSelectScreenScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                controller.handleExitRequest();
            }
            if(event.getCode() == KeyCode.H && event.isControlDown())
            {
                controller.handleHomeButtonRequest();
            }
            if(event.getCode() == KeyCode.L && event.isControlDown())
            {
                controller.handleLogoutRequest();
            }
        });
    }

}
