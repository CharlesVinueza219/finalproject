package gui;

import controller.BuzzwordController;
import controller.InGameController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;

import static javafx.scene.paint.Color.rgb;

/**
 * @author Charles Vinueza
 */

public class GameScreenGUI {
    static Scene gameScreenScene;
    static boolean paused = false; //indicates whether game is paused or not
    int level;
    int targetScore;
    InGameController gameController = new InGameController();
    BuzzwordController buzzController = new BuzzwordController();
    GridPane grid; // grid of buttons
    GridPane gridPane;
    ListView listView;
    Image playIcon; //play / pause button that is changed when clicked
    ImageView play; // imageview of the play/pause image
    ObservableList<Node> letterList;
    ArrayList<Button> buttonList = new ArrayList<>();
    ArrayList<CustomButton> customButtons = new ArrayList<>();
    ArrayList<Character> wordList = new ArrayList<>();
    ObservableList names;
    Label sumLabel;
    Button saveButton;
    Button replayButton;
    Button playNextLevelButton;

    DropShadow shadow = new DropShadow(); //button effects when hovered
    String category;
    Button pauseButton;
    Label target;
    static Label timeRemainingLabel;
    static boolean mouseDragReleased = false;
    boolean draggingEnabled = true;

    public GameScreenGUI(String category, int level) {
        this.category = category;
        this.level = level;
        initLevelSelectScreenScene(category, level);
    }

    public static Scene getScene() {
        return gameScreenScene;
    }

    public void initLevelSelectScreenScene(String cat, int level) {
        BorderPane borderPane = new BorderPane(); //main borderpane in the game screen

        Image backarrow = new Image("images/backarrow.png");
        ImageView logoutImg = new ImageView(backarrow);
        logoutImg.setFitHeight(15);
        logoutImg.setFitWidth(15);
        Button profileButton = new Button(FirstSceneGUI.getGameData().getUsrName().toString(), logoutImg);
        //Setting up profile button
        profileButton.setPrefWidth(175);
        profileButton.setPrefHeight(40);
        profileButton.setOnMouseClicked(e -> gameController.handleLogoutRequest());
        profileButton.setStyle("-fx-background-color:  #9eb8e0");
        profileButton.setOnMouseEntered(e -> profileButton.setEffect(shadow));
        profileButton.setOnMouseExited(e -> profileButton.setEffect(null));

        //Home Button
        Button homeButton = new Button();
        homeButton.setText("Home");
        homeButton.setPrefWidth(175);
        homeButton.setPrefHeight(40);
        homeButton.setOnMouseClicked(event -> gameController.handleHomeButtonRequest());
        homeButton.setStyle("-fx-background-color:  #9eb8e0");
        homeButton.setOnMouseEntered(e -> homeButton.setEffect(shadow));
        homeButton.setOnMouseExited(e -> homeButton.setEffect(null));


        replayButton = new Button();
        replayButton.setText("Replay Level");
        replayButton.setPrefWidth(175);
        replayButton.setPrefHeight(40);
        replayButton.setOnMouseClicked(event -> gameController.handleReplay());
        replayButton.setStyle("-fx-background-color:  #9eb8e0");
        replayButton.setOnMouseEntered(e -> replayButton.setEffect(shadow));
        replayButton.setOnMouseExited(e -> replayButton.setEffect(null));

        saveButton = new Button();
        saveButton.setText("Save Progress");
        saveButton.setPrefWidth(175);
        saveButton.setPrefHeight(40);
        saveButton.setOnMouseClicked(event -> gameController.handleInGameSaveRequest());
        saveButton.setStyle("-fx-background-color:  #9eb8e0");
        saveButton.setOnMouseEntered(e -> saveButton.setEffect(shadow));
        saveButton.setOnMouseExited(e -> saveButton.setEffect(null));
        saveButton.setVisible(false);

        playNextLevelButton = new Button();
        playNextLevelButton.setText("Play Next Level");
        playNextLevelButton.setPrefWidth(175);
        playNextLevelButton.setPrefHeight(40);
        playNextLevelButton.setOnMouseClicked(event -> gameController.playNextLevel());
        playNextLevelButton.setStyle("-fx-background-color:  #9eb8e0");
        playNextLevelButton.setOnMouseEntered(e -> playNextLevelButton.setEffect(shadow));
        playNextLevelButton.setOnMouseExited(e -> playNextLevelButton.setEffect(null));
        playNextLevelButton.setVisible(false);


        VBox toolbarBox = new VBox();
        toolbarBox.setPrefWidth(175);
        toolbarBox.setPadding(new Insets(220, 0, 0, 0));
        toolbarBox.setMargin(profileButton, new Insets(0, 0, 25, 0));
        toolbarBox.setMargin(homeButton, new Insets(0, 0, 25, 0));
        toolbarBox.setMargin(replayButton, new Insets(0, 0, 25, 0));
        toolbarBox.setMargin(saveButton, new Insets(0, 0, 25, 0));



        //Add both buttons to the toolbar and adjust their position with padding
        toolbarBox.getChildren().addAll(profileButton, homeButton, replayButton, saveButton, playNextLevelButton);

        borderPane.setLeft(toolbarBox); //set toolbar box on the left side of the borderpane
        borderPane.getLeft().setStyle("-fx-background-color: #c5d4ed;");


        VBox main = new VBox(); // main pain next to toolbar


        HBox titleBox = new HBox();
        Text title = new Text();
        title.setFont(new Font("Times New Roman", 50));
        title.setText("!!Buzzword!!");
        title.setFill(rgb(197, 212, 237));
        titleBox.getChildren().add(title); //add Buzzword title to main VBox

        HBox categoryTitleBox = new HBox();
        Text categoryTitle = new Text();
        categoryTitle.setFont(new Font("Times New Roman", 35));
        //category title HBox
        categoryTitle.setText(cat);
        categoryTitle.setUnderline(true);
        categoryTitle.setFill(rgb(235, 240, 249));
        categoryTitleBox.getChildren().addAll(categoryTitle);
        categoryTitleBox.setPadding(new Insets(15, 0, 0, 0));

        HBox levelNumberBox = new HBox();
        Text levelNum = new Text();
        levelNum.setUnderline(true);
        levelNum.setText("Level " + level + "");
        levelNum.setFont(new Font("Times new Roman", 20));
        levelNum.setFill(Color.rgb(235, 240, 249));
        levelNumberBox.getChildren().add(levelNum);

        HBox buttonBox = new HBox();
        playIcon = new Image("images/pauseicon.png");
        play = new ImageView(playIcon);

        play.setFitHeight(60);
        play.setFitWidth(60);
        pauseButton = new Button();
        pauseButton.setGraphic(play);
        pauseButton.setStyle("-fx-background-color: transparent;");

        //play/pause eventHandlers
        pauseButton.setOnMouseClicked(e ->
        {
            if(paused == false)
            {
                pause();
            }
            else
            {
                play();
            }
        });
        buttonBox.getChildren().addAll(pauseButton);


        //rightBox for the game stats
        VBox rightBox = new VBox();
        rightBox.setPrefWidth(215);
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        //add close button into rightBox
        HBox closeButtonBox = new HBox();
        Image imageOk = new Image("images/CloseCross.png");
        ImageView btnImage = new ImageView(imageOk);
        btnImage.setFitHeight(20);
        btnImage.setFitWidth(20);

        //add closeButton functionality and icon
        Button closeButton = new Button("", btnImage);
        closeButton.setStyle("-fx-background-color: transparent;");
        closeButtonBox.getChildren().add(closeButton);
        closeButton.setOnMouseEntered(e -> closeButton.setEffect(shadow));
        closeButton.setOnMouseExited(e -> closeButton.setEffect(null));
        closeButton.setOnMouseClicked(e -> gameController.handleExitRequest());

        //time Remaining Box (will need to be updated once a timer is implemented
        HBox timeRemainingBox = new HBox();
        timeRemainingLabel = new Label("Time Remaining: 60 seconds");
        timeRemainingLabel.setFont(Font.font("Times New Roman", 15));
        timeRemainingBox.setStyle("-fx-background-color: #EBF0F9;" + "-fx-background-radius: 5.0;" + "-fx-spacing: 5; -fx-padding: 5;");
        timeRemainingBox.setPadding(new Insets(0, 40, 0, 0));
        timeRemainingBox.getChildren().add(timeRemainingLabel);
        timeRemainingBox.setAlignment(Pos.CENTER);
        gridPane.add(timeRemainingBox, 0, 11);

        //currentLetters HBox which displays the letters being scrolled over
        HBox currentLetters = new HBox();
        currentLetters.setPrefHeight(35);
        letterList= currentLetters.getChildren();
        currentLetters.setStyle("-fx-background-color: #EBF0F9;" + "-fx-background-radius: 5.0;" + "-fx-spacing: 5; -fx-padding: 5;");
        currentLetters.setPadding(new Insets(0, 0, 0, 20));
        gridPane.add(currentLetters,0 ,12);

        names = FXCollections.observableArrayList();

        StackPane listViewBox = new StackPane();
        GridPane gridInsidePane = new GridPane();
        gridInsidePane.setVgap(1);
        gridInsidePane.setHgap(10);
        listView = new ListView();
        listView.setPrefSize(190, 150);
        listView.setEditable(true);
        sumLabel = new Label();
        sumLabel.setText("SUM: \t\t\t" + names.size() * 4);
        sumLabel.setFont(Font.font("Times New Roman", 18));
        sumLabel.setStyle("-fx-background: red");

        listView.setItems(names);

        gridInsidePane.add(listView, 0, 0);
        gridInsidePane.add(sumLabel, 0, 1);
        listViewBox.getChildren().add(gridInsidePane);

        gridPane.add(listViewBox, 0, 16);

        rightBox.getChildren().addAll(closeButtonBox, gridPane);
        closeButtonBox.setAlignment(Pos.TOP_RIGHT);
        main.setStyle("-fx-background-color:  #779bd4");
        main.getChildren().addAll(titleBox, categoryTitleBox);


        titleBox.setAlignment(Pos.CENTER);
        categoryTitleBox.setAlignment(Pos.CENTER);

        borderPane.setCenter(main);
        main.setPadding(new Insets(25, 0, 0, 0));
        borderPane.setRight(rightBox);
        rightBox.setStyle("-fx-background-color: #779bd4;");

        //button grid
        grid = new GridPane();
        grid.setHgap(11);
        grid.setVgap(11);

        initButtons(level, cat);

        grid.setPadding(new Insets(15, 0, 0, 0));
        grid.setAlignment(Pos.CENTER);
        main.getChildren().addAll(grid, levelNumberBox, buttonBox); // Add nodes under grid

        buttonBox.setAlignment(Pos.CENTER);
        levelNumberBox.setAlignment(Pos.CENTER);
        levelNumberBox.setPadding(new Insets(15, 0, 0, 0));

        gameScreenScene = new Scene(borderPane, 900, 650);
        GameScreenGUI.getScene().addEventFilter(MouseEvent.DRAG_DETECTED , new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                GameScreenGUI.getScene().startFullDrag();
            }
        });

        if(level <  FirstSceneGUI.getGameData().getNumLevelsUnlocked(cat))
        {
            playNextLevelButton.setVisible(true);
        }
        gameController.play(cat, level);

    }

    public void initButtons(int level, String category) {
        int num = 1;
        char[][] letterArr = gameController.generateLetterGrid(category, level);
        for (int i = 0; i < letterArr[0].length; i++) {
            for (int j = 0; j < letterArr[0].length; j++) {
                Button gridButton = new Button(String.valueOf(letterArr[i][j]));
                gridButton.setStyle(
                        "-fx-background-radius: 75em; " +
                                "-fx-min-width: 75px; " +
                                "-fx-min-height: 75px; " +
                                "-fx-max-width: 75px; " +
                                "-fx-max-height: 75px;" +
                                "-fx-background-color: #c5d4ed; " +
                                "-fx-font: 22 Times_New_Roman;"

                );
                gridButton.setOnMouseDragEntered(e ->
                {
                    if(draggingEnabled) {
                        gameController.setTypingEnabled(false);
                        if (letterList.isEmpty()) {
                            buttonList.add(gridButton);
                            pathColorUpdate(gridButton);
                            letterList.add(new LetterUsed(gridButton.getText().charAt(0)));
                            wordList.add(gridButton.getText().charAt(0));

                        } else {
                            {
                                int rowIndexButton2 = GridPane.getRowIndex(gridButton);
                                int rowIndexButton1 = GridPane.getRowIndex(buttonList.get(buttonList.size() - 1));
                                int colIndexButton2 = GridPane.getColumnIndex(gridButton);
                                int colIndexButton1 = GridPane.getColumnIndex(buttonList.get(buttonList.size() - 1));

                                if ((Math.pow(rowIndexButton2 - rowIndexButton1, 2)) + Math.pow(colIndexButton2 - colIndexButton1, 2) <= 2 &&
                                        buttonList.contains(gridButton) == false) {
                                    mouseDragReleased = false;
                                    buttonList.add(gridButton);
                                    pathColorUpdate(gridButton);
                                    letterList.add(new LetterUsed(gridButton.getText().charAt(0)));
                                    wordList.add(gridButton.getText().charAt(0));

                                }
                            }
                        }
                    }
                });

                gridButton.setOnMouseReleased(e -> {
                    if(draggingEnabled) {
                        mouseDragReleased = true;
                        gameController.setGuessedWordDrag(wordList);
                        pathEnded(buttonList);
                        buttonList.clear();
                        letterList.clear();
                        wordList.clear();
                        gameController.setTypingEnabled(true);

                    }
                });


                grid.add(gridButton, i, j);
                CustomButton cst = new CustomButton(gridButton, j, i, gridButton.getText().charAt(0));
                customButtons.add(cst);
            }
        }
        for(int k = 0;k<customButtons.size();k++)
        {
            customButtons.get(k).addAdjancentButtons(grid);
        }

    }


    //Class to display the observable list
    public static class LetterUsed extends StackPane
    {
        private Text text;
        public LetterUsed(char letter) {
            text = new Text(String.valueOf(letter).toUpperCase());
            text.setFont(new Font("Times New Roman", 20));
            text.setFill(Color.BLACK);
            text.setVisible(true);
            setAlignment(Pos.CENTER);
            getChildren().addAll(text);
        }
    }
    //update CSS of the selected buttons
    public void pathColorUpdate(Button button)
    {
        button.setStyle("-fx-background-radius: 75em; " +
                "-fx-min-width: 75px; " +
                "-fx-min-height: 75px; " +
                "-fx-max-width: 75px; " +
                "-fx-max-height: 75px;" +
                "-fx-background-color: #00ff00;" +
                "-fx-font: 22 Times_New_Roman;");
    }
    //revert buttons to their original state
    public void pathEnded(ArrayList btnList)
    {
        for(int i = 0; i<btnList.size();i++)
        {
            Button btn = (Button) btnList.get(i);
            btn.setStyle("-fx-background-radius: 75em; " +
                    "-fx-min-width: 75px; " +
                    "-fx-min-height: 75px; " +
                    "-fx-max-width: 75px; " +
                    "-fx-max-height: 75px;" +
                    "-fx-background-color: #c5d4ed; " +
                    "-fx-font: 22 Times_New_Roman;");
        }

    }

    public GridPane getGrid()
    {
        return grid;
    }
    public void pause()
    {
        paused = true;
        playIcon = new Image("images/playicon.png");
        play = new ImageView(playIcon);
        play.setFitHeight(60);
        play.setFitWidth(60);
        pauseButton.setGraphic(play);
        gameController.handlePauseRequest();
    }
    public void play()
    {
        paused = false;
        playIcon = new Image("images/pauseicon.png");
        play = new ImageView(playIcon);
        play.setFitHeight(60);
        play.setFitWidth(60);
        pauseButton.setGraphic(play);
        gameController.handlePlayRequest();
    }
    public void setTargetLabel(int targtScore)
    {
        targetScore = targtScore;
        StringBuffer targetScoreString = new StringBuffer("Target: \n\n" + targetScore + " points\t\t");
        target = new Label();
        target.setText(targetScoreString.toString());
        target.setStyle("-fx-background-color: #EBF0F9;" + "-fx-background-radius: 10.0;" + "-fx-spacing: 5; -fx-padding: 5;");
        // target.setWrapText(true);
        target.setAlignment(Pos.CENTER);
        gridPane.add(target,0 ,20);

    }
    public void addLossKeyEvents()
    {
        gameScreenScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.H && event.isControlDown())
            {
                gameController.handleHomeButtonRequest();
            }
            if(event.getCode() == KeyCode.R && event.isControlDown())
            {
                gameController.handleReplay();
            }
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                gameController.handleExitRequest();
            }
        });
    }
    public void removeSaveKeyEvents()
    {
        //playNextLevel
        //Home
        //Logout
        //Quit
        //Replay
        gameScreenScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.RIGHT && event.isShiftDown())
            {
                gameController.playNextLevel();
            }
            if(event.getCode() == KeyCode.H && event.isControlDown())
            {
                gameController.handleHomeButtonRequest();
            }
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                gameController.handleExitRequest();
            }
            if(event.getCode() == KeyCode.R && event.isControlDown())
            {
                gameController.handleReplay();
            }
            if(event.getCode() == KeyCode.L && event.isControlDown())
            {
                gameController.handleLogoutRequest();
            }

        });
    }
    public void addGameWonAndSavableKeyEvents()
    {
        //playNextLevel
        //Home
        //Logout
        //Quit
        //Replay
        //save
        gameScreenScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.RIGHT && event.isShiftDown())
            {
                gameController.playNextLevel();
            }
            if(event.getCode() == KeyCode.H && event.isControlDown())
            {
                gameController.handleHomeButtonRequest();
            }
            if(event.getCode() == KeyCode.Q && event.isControlDown())
            {
                gameController.handleExitRequest();
            }
            if(event.getCode() == KeyCode.R && event.isControlDown())
            {
                gameController.handleReplay();
            }
            if(event.getCode() == KeyCode.L && event.isControlDown())
            {
                gameController.handleLogoutRequest();
            }
            if(event.getCode() == KeyCode.S && event.isControlDown())
            {
                gameController.handleInGameSaveRequest();
            }
        });
    }

    public static Label getTimeRemainingLabel()
    {
        return timeRemainingLabel;
    }
    public static boolean getMouseDraggedReleased()
    {
        return mouseDragReleased;
    }
    public ObservableList getNames()
    {
        return names;
    }

    public Label getSumLabel() {
        return sumLabel;
    }

    public int getLevel()
    {
        return level;
    }
    public String getCategory()
    {
        return category;
    }

    public Button getSaveButton()
    {
        return saveButton;
    }
    public Button getPlayNextLevelButton()
    {
        return playNextLevelButton;
    }
    public Scene getGameScreenScene()
    {
        return gameScreenScene;
    }
    public ObservableList getLetterList()
    {
        return letterList;
    }
    public void disableDragging()
    {
        draggingEnabled = false;
    }
    public void enableDragging()
    {
        draggingEnabled = true;
    }
    public ArrayList getCustomButtons()
    {
        return customButtons;
    }
    public ArrayList getButtonList()
    {
        return buttonList;
    }

    public ListView getListView() {
        return listView;
    }
}



