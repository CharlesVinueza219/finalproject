package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Charlie on 11/30/16.
 */
public class SetPasswordSingleton extends Stage{

    static SetPasswordSingleton singleton = null;

    String selection;
    PasswordField pwBox;
    Button closeButton;

    private SetPasswordSingleton() {
    }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static SetPasswordSingleton getSingleton() {
        if (singleton == null)
            singleton = new SetPasswordSingleton();
        return singleton;
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Set New Account Password");
        scenetitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label pw = new Label("New Password:");
        grid.add(pw, 0, 1);

        pwBox = new PasswordField();
        grid.add(pwBox, 1, 1);

        Button submitButton = new Button("Submit");
        closeButton = new Button("Close");

        HBox hbBtn = new HBox(10);

        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(submitButton, closeButton);
        grid.add(hbBtn, 1, 4);

        EventHandler<ActionEvent> inputHandler = event -> {
            SetPasswordSingleton.this.selection = ((Button) event.getSource()).getText();
            SetPasswordSingleton.this.close();

        };

        submitButton.setOnAction(inputHandler);
        closeButton.setOnAction(inputHandler);
        Scene scene = new Scene(grid, 300, 250);
        this.setScene(scene);
    }

    public String getSelection()
    {
        return selection;
    }

    public void show(String title)
    {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);


        // SET THE MESSAGE TO DISPLAY TO THE USER
        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
    public void closeDialog()
    {
        this.pwBox.clear();
        this.close();
    }
    public String getNewPassword()
    {
        return pwBox.getText();
    }

}

