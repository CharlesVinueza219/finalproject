package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * @author Charles Vinueza
 */
public class InputDialogSingleton extends Stage {

    static InputDialogSingleton singleton = null;

    String selection;

    PasswordField pwBox;
    TextField userTextField;
    Button closeButton;
    KeyCode keyPressedInScene;


    private InputDialogSingleton() {
    }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static InputDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new InputDialogSingleton();
        return singleton;
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.APPLICATION_MODAL);
        initOwner(owner);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Login");
        scenetitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Button btn = new Button("Submit");
        closeButton = new Button("Close");

        HBox hbBtn = new HBox(10);

        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn, closeButton);
        grid.add(hbBtn, 1, 4);

        EventHandler<ActionEvent> inputHandler = event -> {
            InputDialogSingleton.this.selection = ((Button) event.getSource()).getText();
            InputDialogSingleton.this.close();

        };
        setOnCloseRequest( e -> {

            this.clearInput();
            this.close();
        });
        btn.setOnAction(inputHandler);
        closeButton.setOnAction(inputHandler);
        Scene scene = new Scene(grid, 300, 250);
        this.setScene(scene);
        scene.setOnKeyPressed(event -> {
            keyPressedInScene = event.getCode();
        });
    }

    public String getSelection()
    {
        return selection;
    }

    public void show(String title)
    {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);


        // SET THE MESSAGE TO DISPLAY TO THE USER
        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
    public void showIncorrect(String title) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);
        this.pwBox.clear();
        this.userTextField.clear();

        // SET THE MESSAGE TO DISPLAY TO THE USER
        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        show();
    }
    public String getPassword()
    {
        return pwBox.getText();
    }
    public String getUserName()
    {
        return userTextField.getText();
    }
    public void clearInput()
    {
        this.pwBox.clear();
        this.userTextField.clear();
    }
    public KeyCode getKeyPressedInScene()
    {
        return keyPressedInScene;
    }
}
