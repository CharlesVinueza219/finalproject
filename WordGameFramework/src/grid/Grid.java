package grid;

import java.util.List;
import java.util.stream.Collectors;

public class Grid
{
    BoardGenerator generator;
    Solver solver;
    String category;
    List<String> wordsInBoard;

    public Grid(String category)
    {
        this.category = category;
        generator = new BoardGenerator();
        solver = new Solver(4, generator.getBoard(), category);
        solver.solve();

        //remove duplicates from the generated list of words
        solver.listOfWords =  solver.listOfWords.stream().distinct().collect(Collectors.toList());

        this.wordsInBoard = solver.listOfWords;


        while(solver.listOfWords.isEmpty()) {
            generator = new BoardGenerator();
            solver = new Solver(4, generator.getBoard(), category);
            solver.listOfWords = solver.listOfWords.stream().distinct().collect(Collectors.toList());
        }

        System.out.println("Shouldn't have duplicates: " + solver.listOfWords.toString());

    }
    public static void main(String[] args)
    {
        String category = "threeLetterLevels";
        BoardGenerator generator = new BoardGenerator();
        Solver solver = new Solver(4, generator.getBoard(), category);
        solver.solve();
        System.out.println(generator.getBoard());
        System.out.println("List of Words" + solver.listOfWords.toString());

    }

    public BoardGenerator getGenerator() {
        return generator;
    }

    public String getCategory() {
        return category;
    }

    public List<String> getWordsInBoard()
    {
        return wordsInBoard;
    }

    public BoardGenerator getBoard()
    {
        return generator;
    }
}