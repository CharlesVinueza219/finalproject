package grid;

public class Constants
{
    public static final int LETTERS_IN_ALPHABET = 26;
    public static final int MINIMUM_WORD_LENGTH = 3;
}