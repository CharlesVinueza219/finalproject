package grid;

import java.util.Random;

/**
 * Created by Charlie on 11/19/16.
 */
public class BoardGenerator
{
    String board;

    BoardGenerator()
    {
        board = generateBoard();
    }
    public String generateBoard()
    {
        StringBuffer boardString = new StringBuffer("");
        Random rand = new Random();
        int combo = rand.nextInt((2 - 1) + 1) + 1; //rand.nextInt((max - min) + 1) + min;
        switch (combo) {
            case 1:
                boardString = combo1();
                break;
            case 2:
                boardString = combo2();
                break;
        }
        return boardString.toString();

    }
    public static void main(String[] args)
    {
        BoardGenerator h = new BoardGenerator();
    }

    public String getBoard() {
        return board;
    }

    public char drawConsonant()
    {
        Random rand = new Random();
        char[] charArray = {'S','S','S','T','T','T','W','W','R','R','N','N', 'N',
                            'L','L','H','H','C', 'C', 'U', 'U', 'D',
                            'D','M','M','J','K', 'Y', 'B', 'P', 'G',
                            'F','X','Q','Z'}; //more probability to select a more common consonant
        char chosenChar = charArray[rand.nextInt(charArray.length-1)];
        return chosenChar;
    }
    public char drawVowel()
    {
        Random rand = new Random();
        char[] charArray = {'A','E','E','I','O','U'};
        char chosenChar = charArray[rand.nextInt(charArray.length-1)];
        return chosenChar;
    }

    public StringBuffer combo1()
    {
        Random rand = new Random();
        StringBuffer toReturn = new StringBuffer("");

        //make row 1 by choosing 1 vowel, choosing a random position
        //for it between 0 - 3 and fill the rest with consonants

        char[] chars = new char[16];

        int firstRowVowelPos = rand.nextInt((3 - 0) + 1) + 0;
        chars[firstRowVowelPos] = drawVowel();
        for(int i = 0; i< 4;i++)
        {
            if(!(i == firstRowVowelPos))
            {
                chars[i] = drawConsonant();
            }
        }

        //make row 2 by choosing 2 vowels, choosing a random position
        //for them between 4 - 7 and fill the rest with consonants

        int secondRowVowel1Pos = rand.nextInt((7 - 4) + 1) + 4;
        chars[secondRowVowel1Pos] = drawVowel();

        int secondRowVowel2Pos = secondRowVowel1Pos;
        while(secondRowVowel2Pos == secondRowVowel1Pos)
        {
            secondRowVowel2Pos = rand.nextInt((7 - 4) + 1) + 4;

        }
        chars[secondRowVowel2Pos] = drawVowel();

        for(int i = 4; i< 8;i++)
        {
            if(!(i == secondRowVowel1Pos) && !(i == secondRowVowel2Pos))
            {
                chars[i] = drawConsonant();
            }
        }

        //make row 3 with the same logic as row 2 (i.e. 2 vowels)

        int thirdRowVowel1Pos = rand.nextInt((11 - 8) + 1) + 8;
        chars[thirdRowVowel1Pos] = drawVowel();

        int thirdRowVowel2Pos = thirdRowVowel1Pos;
        while(thirdRowVowel2Pos == thirdRowVowel1Pos)
        {
            thirdRowVowel2Pos = rand.nextInt((11 - 8) + 1) + 8;

        }
        chars[thirdRowVowel2Pos] = drawVowel();

        for(int i = 8; i< 12;i++)
        {
            if(!(i == thirdRowVowel1Pos) && !(i == thirdRowVowel2Pos))
            {
                chars[i] = drawConsonant();
            }
        }
        //make row 4 with the same logic as rows 2 and 3
        int fourthRowVowel1Pos = rand.nextInt((15 - 12) + 1) + 12;
        chars[fourthRowVowel1Pos] = drawVowel();

        int fourthRowVowel2Pos = fourthRowVowel1Pos;
        while(fourthRowVowel2Pos == fourthRowVowel1Pos)
        {
            fourthRowVowel2Pos = rand.nextInt((15 - 12) + 1) + 12;

        }
        chars[fourthRowVowel2Pos] = drawVowel();

        for(int i = 12; i< 16;i++)
        {
            if(!(i == fourthRowVowel1Pos) && !(i == fourthRowVowel2Pos))
            {
                chars[i] = drawConsonant();
            }
        }

        toReturn.append(chars); //convert char array to the StringBuffer

        return toReturn;
    }

    public StringBuffer combo2()
    {
        Random rand = new Random();
        StringBuffer toReturn = new StringBuffer("");

        //make row 1 by choosing 2 vowels, choosing a random position
        //for them between 0 - 3 and fill the rest with consonants

        char[] chars = new char[16];

        int firstRowVowel1Pos = rand.nextInt((3 - 0) + 1) + 0;
        chars[firstRowVowel1Pos] = drawVowel();

        int firstRowVowel2Pos = firstRowVowel1Pos;
        while(firstRowVowel1Pos == firstRowVowel2Pos)
        {
            firstRowVowel2Pos = rand.nextInt((3 - 0) + 1) + 0;

        }
        chars[firstRowVowel2Pos] = drawVowel();

        for(int i = 0; i< 4;i++)
        {
            if(!(i == firstRowVowel1Pos) && !(i == firstRowVowel2Pos))
            {
                chars[i] = drawConsonant();
            }
        }

        //make row 2 by choosing 2 vowels, choosing a random position
        //for them between 4 - 7 and fill the rest with consonants

        int secondRowVowel1Pos = rand.nextInt((7 - 4) + 1) + 4;
        chars[secondRowVowel1Pos] = drawVowel();

        int secondRowVowel2Pos = secondRowVowel1Pos;
        while(secondRowVowel2Pos == secondRowVowel1Pos)
        {
            secondRowVowel2Pos = rand.nextInt((7 - 4) + 1) + 4;

        }
        chars[secondRowVowel2Pos] = drawVowel();

        for(int i = 4; i< 8;i++)
        {
            if(!(i == secondRowVowel2Pos) && !(i == secondRowVowel1Pos))
            {
                chars[i] = drawConsonant();
            }
        }

        //make row 3 with 3 vowels

        int thirdRowVowel1Pos = rand.nextInt((11 - 8) + 1) + 8;
        chars[thirdRowVowel1Pos] = drawVowel();

        int thirdRowVowel2Pos = thirdRowVowel1Pos;
        while(thirdRowVowel2Pos == thirdRowVowel1Pos)
        {
            thirdRowVowel2Pos = rand.nextInt((11 - 8) + 1) + 8;

        }
        chars[thirdRowVowel2Pos] = drawVowel();


        for(int i = 8; i< 12;i++)
        {
            if(!(i == thirdRowVowel1Pos) && !(i == thirdRowVowel2Pos))
            {
                chars[i] = drawConsonant();
            }
        }

        //make row 4 with 1 vowel
        int fourthRowVowel1Pos = rand.nextInt((15 - 12) + 1) + 12;
        chars[fourthRowVowel1Pos] = drawVowel();

        for(int i = 12; i< 16;i++)
        {
            if(!(i == fourthRowVowel1Pos))
            {
                chars[i] = drawConsonant();
            }
        }
        toReturn.append(chars); //convert char array to the StringBuffer

        return toReturn;
    }
}
