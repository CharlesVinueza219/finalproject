package grid;

import java.util.LinkedList;
import java.util.List;

public class Solver
{
    private char[] puzzle;
    private int maxSize;

    private boolean[] used;
    private StringBuilder stringSoFar;

    private boolean[][] matrix;
    private Trie trie;

    List<String> listOfWords = new LinkedList<>();
    private String category;

    public Solver(int size, String puzzle, String category)
    {
        this.category = category;
        trie = Util.getTrie(size);
        matrix = Util.connectivityMatrix(size);

        maxSize = size * size;
        stringSoFar = new StringBuilder(maxSize);
        used = new boolean[maxSize];

        if (puzzle.length() == maxSize)
        {
            this.puzzle = puzzle.toCharArray();
        }
        else
        {
            System.out.println("The puzzle size does not match the size specified: " + puzzle.length());
            this.puzzle = puzzle.substring(0, maxSize).toCharArray();
        }
    }

    private void traverseAt(int origin)
    {
        stringSoFar.append(puzzle[origin]);
        used[origin] = true;

        //Check if we have a valid word
        if(category.equalsIgnoreCase("Three Letter Words")) {
            if ((stringSoFar.length() == Constants.MINIMUM_WORD_LENGTH) && (trie.containKey(stringSoFar.toString()))) {
                listOfWords.add(stringSoFar.toString());
            }

            //Find where to go next
            for (int destination = 0; destination < maxSize; destination++) {
                if (matrix[origin][destination] && !used[destination] && trie.containPrefix(stringSoFar.toString() + puzzle[destination])) {
                    traverseAt(destination);
                }
            }

            used[origin] = false;
            stringSoFar.deleteCharAt(stringSoFar.length() - 1);
        }

        else if(category.equalsIgnoreCase("Four Letter Words"))
        {
            if ((stringSoFar.length() >= Constants.MINIMUM_WORD_LENGTH) && (stringSoFar.length() == 4) && (trie.containKey(stringSoFar.toString()))) {
                listOfWords.add(stringSoFar.toString());
            }

            //Find where to go next
            for (int destination = 0; destination < maxSize; destination++) {
                if (matrix[origin][destination] && !used[destination] && trie.containPrefix(stringSoFar.toString() + puzzle[destination])) {
                    traverseAt(destination);
                }
            }

            used[origin] = false;
            stringSoFar.deleteCharAt(stringSoFar.length() - 1);
        }
        else
        {
            if ((stringSoFar.length() >= Constants.MINIMUM_WORD_LENGTH) && (stringSoFar.length() == 5) && (trie.containKey(stringSoFar.toString()))) {
                listOfWords.add(stringSoFar.toString());
            }

            //Find where to go next
            for (int destination = 0; destination < maxSize; destination++) {
                if (matrix[origin][destination] && !used[destination] && trie.containPrefix(stringSoFar.toString() + puzzle[destination])) {
                    traverseAt(destination);
                }
            }

            used[origin] = false;
            stringSoFar.deleteCharAt(stringSoFar.length() - 1);
        }
    }
    public void solve()
    {
        for (int i = 0; i < maxSize; i++)
        {
            traverseAt(i);
        }
    }

    public List<String> getListOfWords() {
        return listOfWords;
    }
}